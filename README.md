# ТЕДЕРА #

* Европейская процедура [openeu.api-docs](http://openeu.api-docs.openprocurement.org/uk/latest/overview.html)
* Переговорная процедура [limited.api-docs](http://limited.api-docs.openprocurement.org/uk/latest/overview.html)
* Процедура для обороны [limited.api-docs](http://defense.api-docs.openprocurement.org/uk/latest/overview.html)
* Конкурентний діалог [competitivedialogue.api-docs](http://competitivedialogue.api-docs.openprocurement.org/uk/latest/overview.html)
* ECKO [esco.api-docs](http://esco.api-docs.openprocurement.org/uk/latest/overview.html)


# ПЛАНЫ #

* Основная документация [planning.api-docs.openprocurement.org](http://planning.api-docs.openprocurement.org/uk/latest/overview.html)


# АУКЦИОНЫ #

* ЦБД 1 [dgf.api-docs](http://dgf.api-docs.openprocurement.org/en/latest/standard/auction.html), [ТЗ.g-docs](https://docs.google.com/document/d/14Yav0iwWOHd-6NeZ_P4fHlNT8e22Hlo24RVz6rSed4M/edit)
* ЦБД 2 -
* MP - [api](https://openprocurementauctionsswiftsure.readthedocs.io/en/latest/overview.html)


# РЕЕСТР АКТИВОВ ЦБД1 DGF(ФГВФО) #

* BasicAssets [basicassets.api-docs](http://basicassets.api-docs.registry.ea.openprocurement.io/en/latest/)
* ClaimRights [claimrights.api-docs](http://claimrights.api-docs.registry.ea.openprocurement.io/en/latest/)
* BasicLots [basiclots.api-docs](http://basiclots.api-docs.registry.ea.openprocurement.io/projects/translation/uk/latest/overview.html)


# РЕЕСТР АКТИВОВ ЦБД2 (Приватизация) #

* AssetsBounce [assetsbounce.api-docs](http://assetsbounce.api-docs.registry.ea2.openprocurement.io/en/latest/overview.html), [ТЗ.g-docs](https://docs.google.com/document/d/1yqQvFCquJAMqoZFXcwR-oQ0eyTLyFioOplnhHLAJZ5U/edit#heading=h.pd05ozcpaz40), [ToR.g-docs](https://docs.google.com/document/d/1qufxGq7b9w1ZgdSKrXP4DQqO_2641hNKriJN_hE4CYU/edit?ts=5aeb08bf#heading=h.j1pezr9tr1ht)
